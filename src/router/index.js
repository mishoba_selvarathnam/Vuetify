import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Portfolio from "@/views/Portfolio.vue";
import Service from "@/views/Service.vue";
import Resume from "@/views/Resume.vue";
import Blog from "@/views/Blog.vue";
import Contact from "@/views/Contact.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/portfolio",
    name: "Portfolio",
    component: Portfolio
  },
  {
    path: "/service",
    name: "Service",
    component: Service
  },
  {
    path: "/resume",
    name: "Resume",
    component: Resume
  },
  {
    path: "/blog",
    name: "Blog",
    component: Blog
  },
  {
    path: "/contact",
    name: "Contact",
    component: Contact
  },
  
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
